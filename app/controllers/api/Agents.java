package controllers.api;

import java.util.HashMap;

import controllers.AppController;
import models.BaseModel;
import models.Agent;
import play.data.validation.Validation;

public class Agents extends AppController {

    public static void list(String query, String paginateField, String paginateDirection, int page) {
        if (!Validation.hasErrors()) {
            final HashMap<String, Object> result = Agent.search(null, null, null, 1);
            renderJSON(result.get(BaseModel.PAGINATE_OBJECT_KEY));
        }
    }

}
