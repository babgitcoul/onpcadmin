package controllers.api;

import java.util.HashMap;

import controllers.AppController;
import models.BaseModel;
import models.Formateur;
import play.data.validation.Validation;

public class Formateurs extends AppController {

    public static void list(String query, String paginateField, String paginateDirection, int page) {
        if (!Validation.hasErrors()) {
            final HashMap<String, Object> result = Formateur.search(null, paginateField, paginateDirection, page);
            renderJSON(result.get(BaseModel.PAGINATE_OBJECT_KEY));
        }
    }

}
