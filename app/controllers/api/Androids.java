package controllers.api;

import java.util.HashMap;

import controllers.AppController;
import models.BaseModel;
import models.AndroidJson;
import play.data.validation.Validation;

public class Androids extends AppController {

    public static void list(String query, String paginateField, String paginateDirection, int page) {
        if (!Validation.hasErrors()) {
            final HashMap<String, Object> result = AndroidJson.search(null, paginateField, paginateDirection, page);
            renderJSON(result.get(BaseModel.PAGINATE_OBJECT_KEY));
        }
    }

}
